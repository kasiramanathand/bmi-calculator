/********* 
JavaScript Coding Challenge 1
*/

var heightMark, weightMark, heightJohn, weightJohn, bmiMark, bmiJohn, result;
heightMark = 1.69; //Meters
heightJohn = 1.95; //Meters
weightMark = 78; //KG
weightJohn = 92; //KG

bmiMark = weightMark / (heightMark * heightMark); //Formula to calculate BMI
bmiJohn = weightJohn / (heightJohn * heightJohn);

result = bmiJohn > bmiMark;

console.log('Is Mark\'s BMI higher than John\'s? ' + result);